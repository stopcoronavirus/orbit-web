import translation from './translation.json'
import dateLocale from 'date-fns/locale/fr'

export default { translation, dateLocale }
