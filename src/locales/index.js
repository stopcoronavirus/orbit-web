import en from './en'
import fr from './fr'
import fi from './fi'
import pt from './pt'
import zh from './zh'

export default {
  en,
  fr,
  fi,
  pt,
  zh
}
